window.onload = () => {
    
    var tours = document.getElementById('tours');
    
    var elements = document.getElementsByClassName('cta');
    
    for (var e=0;e<elements.length;e++){
        
        var links = elements[e].getElementsByTagName('a');
        
        for (var l=0;l<links.length;l++){
            
            links[l].addEventListener("click", (e)=>{
                
                e.preventDefault();
                
                window.scrollTo(0, tours.offsetTop);
            });
        }
    };
    
    var items = tours.getElementsByTagName('li')
    
    for (var i=0;i<items.length;i++){
        
        items[i].addEventListener("click", (e)=>{
            
            e.preventDefault();
            
            let image = e.target.closest('li').getElementsByTagName('img')[0];
            
            document.getElementById('location').value = image.getAttribute('alt');
        });
    };
};